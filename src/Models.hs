{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE ExistentialQuantification  #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
module Models where

import           Control.Monad.Reader
import           Data.Aeson
import           Data.Text
import           Data.Time
import           Database.Persist.Sql
import           Database.Persist.TH
import           GHC.Generics

share
  [mkPersist sqlSettings, mkMigrate "migrateAll"]
  [persistLowerCase|
  Users json
    name Text maxlen=52 sqltype=varchar(52)
    pass Text maxlen=52 sqltype=varchar(52)
    Primary name
    UniqueUsersName name
    deriving Show Eq
  SuperSecrets json
    something Text
    at UTCTime
    by UsersId maxlen=52 sqltype=varchar(52)
    deriving Show Eq
|]

doMigration :: ReaderT SqlBackend IO ()
doMigration = runMigration migrateAll

data AuthUser = AuthUser
  { authName :: Text
  , token    :: Text
  } deriving (Show, Eq, Generic)
data UsersSecret = UsersSecret
  { something :: Text
  } deriving (Show, Generic)

instance FromJSON AuthUser
instance ToJSON AuthUser
instance FromJSON UsersSecret
instance ToJSON UsersSecret
