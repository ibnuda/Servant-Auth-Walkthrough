module DB where

import           Control.Monad.Logger
import           Data.Text              hiding (map)
import           Data.Time
import           Database.Persist
import           Database.Persist.MySQL
import           Models

createPool :: IO ConnectionPool
createPool = runStdoutLoggingT $ createMySQLPool connection 5
  where
    connection =
      defaultConnectInfo
      { connectHost = "localhost"
      , connectPort = fromIntegral 3306
      , connectUser = "ibnu"
      , connectPassword = "jaran"
      , connectDatabase = "owo"
      }

runQuery query = do
  pool <- createPool
  runSqlPool query pool

lookUserByUsernameAndPassword :: Text -> Text -> IO (Maybe Users)
lookUserByUsernameAndPassword username password = do
  mUser <- runQuery $ selectFirst [UsersName ==. username, UsersPass ==. password] []
  return $ fmap entityVal mUser

lookUserByUsername :: Text -> IO (Maybe Users)
lookUserByUsername username = do
  mUser <- runQuery $ selectFirst [UsersName ==. username] []
  return $ fmap entityVal mUser

lookSecretByUsername :: Text -> IO [SuperSecrets]
lookSecretByUsername username = do
  somes <- runQuery $ selectList [SuperSecretsBy ==. (UsersKey username)] []
  return $ map entityVal somes

insertSecret :: Text -> UsersSecret -> IO (Key SuperSecrets)
insertSecret username usersSecret = do
  now <- getCurrentTime
  runQuery $
    insert $ SuperSecrets (something usersSecret) now (UsersKey username)
