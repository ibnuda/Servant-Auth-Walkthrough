{-# LANGUAGE OverloadedStrings #-}
module Auth where

import           Data.Aeson
import           Data.ByteString
import           Data.GUID
import qualified Data.Map              as Map
import           Data.String
import           Data.Text
import           Data.Text.Encoding
import           Data.Time
import           Data.Time.Clock.POSIX
import           Prelude               hiding (exp)
import           Web.JWT

import           Models

nowPosix :: IO POSIXTime
nowPosix = do
  now <- getCurrentTime
  return $ utcTimeToPOSIXSeconds now

createToken :: Users -> IO AuthUser
createToken user = do
  now <- nowPosix
  guid <- genText
  let creation = numericDate $ now
      expiration = numericDate $ now + 60
      claims =
        def
        { exp = expiration
        , iat = creation
        , iss = stringOrURI "issuer"
        , jti = stringOrURI guid
        , sub = stringOrURI "localhost"
        , unregisteredClaims = Map.fromList [("name", String $ usersName user)]
        }
      key = secret "Indonesia Raya"
      token = encodeSigned HS256 key claims
  return $ AuthUser (usersName user) token

createTokenForUser :: Maybe Users -> IO AuthUser
createTokenForUser Nothing     = return $ AuthUser "" ""
createTokenForUser (Just user) = createToken user

decodeTokenHeader :: ByteString -> Maybe (JWT VerifiedJWT)
decodeTokenHeader rawToken = do
  decodedJWT >>= verify (secret "Indonesia Raya")
  where
    (bearer, jwtBase64) = breakOnEnd " " $ decodeUtf8 rawToken
    decodedJWT = Web.JWT.decode jwtBase64

isTokenExpired :: JWT r -> IO Bool
isTokenExpired token = do
  now <- nowPosix
  case ((exp $ claims token), (numericDate now)) of
    (Just expiration, Just now) -> return $ expiration < now
    _                           -> return True

getNameClaimsFromToken :: (FromJSON t, IsString t) => JWT r -> t
getNameClaimsFromToken token =
  case lookup "name" $ Map.toList $ unregisteredClaims $ claims token of
    Nothing -> ""
    Just a  ->
      case fromJSON a of
        Success s -> s
        Error _   -> ""
