{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies      #-}
{-# LANGUAGE TypeOperators     #-}
module Lib where

import           Control.Monad.IO.Class
import           Control.Monad.Error.Class
import           Data.ByteString
import           Data.Text
import           Database.Persist.Sql
import           Network.Wai
import           Network.Wai.Handler.Warp         as Warp
import           Servant
import           Servant.Server.Experimental.Auth

import           Auth
import           DB
import           Models

type instance AuthServerData (AuthProtect "jwt-auth") = Users

type TopSekrit =
       "auth"
       :> ReqBody '[ JSON] Users
       :> Post '[ JSON] AuthUser
  :<|> "secrets"
       :> AuthProtect "jwt-auth"
       :> ReqBody '[ JSON] UsersSecret
       :> Post '[ JSON] (Key SuperSecrets)
  :<|> "secrets"
       :> AuthProtect "jwt-auth"
       :> Capture "username" Text
       :> Get '[ JSON] [SuperSecrets]

secretContext :: Context '[AuthHandler Request Users]
secretContext = mkAuthHandler secretHandler :. EmptyContext
  where
    secretHandler :: Request -> Handler Users
    secretHandler req =
      case lookup "Authorization" (requestHeaders req) of
        Nothing -> throwError err401
        Just token ->
          case decodeTokenHeader token of
            Nothing -> throwError err401
            Just token -> getUserFromToken token
    getUserFromToken token = do
      expired <- liftIO $ isTokenExpired token
      if expired
        then throwError err401
        else do
          maybeUser <- liftIO $ lookUserByUsername . getNameClaimsFromToken $ token
          case maybeUser of
            Nothing -> throwError err401
            Just user -> return user

secretServer :: Server TopSekrit
secretServer = pAuthH :<|> pSecretH :<|> gSecretUserH
  where
    pAuthH :: (MonadIO m) => Users -> m AuthUser
    pAuthH requestFromUser = do
      mUser <-
        liftIO $
        lookUserByUsernameAndPassword (usersName requestFromUser) (usersPass requestFromUser)
      liftIO $ createTokenForUser mUser
    pSecretH :: (MonadIO m) => Users -> UsersSecret -> m (Key SuperSecrets)
    pSecretH users userSecret = do
      key <- liftIO $ insertSecret (usersName users) userSecret
      return key
    gSecretUserH :: (MonadIO m, MonadError ServantErr m) => Users -> Text -> m [SuperSecrets]
    gSecretUserH users username = do
      if (usersName users /= username)
        then throwError err401
        else liftIO $ lookSecretByUsername username

secretProxy :: Proxy TopSekrit
secretProxy = Proxy

secretApp :: IO ()
secretApp = do
  runQuery doMigration
  Warp.run 8000 $ serveWithContext secretProxy secretContext secretServer
